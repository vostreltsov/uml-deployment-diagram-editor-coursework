package uml.diagram.deployment;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.io.Serializable;

/**
 * @author Valeriy Streltsov
 */
public class Artefact implements Serializable {

    private String fValue;
    private int fX;
    private int fY;

    public Artefact(String value) {
        this(value, 0, 0);
    }

    public Artefact(String value, int x, int y) {
        setValue(value);
        setX(x);
        setY(y);
    }

    /**
     * Draws this node using the given context.
     */
    public void draw(Graphics2D context) {
        context.drawString(":" + fValue, fX, fY);
    }

    /**
     * Sets the value.
     */
    public final String getValue() {
        return fValue;
    }

    /**
     * Returns the value.
     */
    public final void setValue(String value) {
        fValue = value;
    }

    /**
     * Sets the absolute x coordinate.
     */
    public final void setX(int x) {
        fX = x;
    }

    /**
     * Returns the absolute x coordinate.
     */
    public final int getX() {
        return fX;
    }

    /**
     * Sets the absolute y coordinate.
     */
    public final void setY(int y) {
        fY = y;
    }

    /**
     * Returns the absolute y coordinate.
     */
    public final int getY() {
        return fY;
    }

    /**
     * Calculates width of the graphical representation.
     */
    public final int calculateWidth(Graphics2D context) {
        FontMetrics fontMetrics = context.getFontMetrics();
        return fontMetrics.stringWidth(fValue);
    }

    /**
     * Calculates height of the graphical representation.
     */
    public final int calculateHeight(Graphics2D context) {
        FontMetrics fontMetrics = context.getFontMetrics();
        return fontMetrics.getHeight();
    }
}
