package uml.diagram.deployment;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 * @author Valeriy Streltsov
 */
public class PanelScene extends JPanel {

    private MainFrame fMainFrame;
    private NodePropertiesFrame fNodePropsFrame;
    private DiagramDeployment fDiagram; // The diagram model.

    // Drag'n'drop stuff.
    private int oldMouseX;
    private int oldMouseY;

    private Point2D fNewConnectionStart;
    private Point2D fNewConnectionEnd;
    private NodeDevice fDraggingDevice;

    private NodeAbstract fSelectedNode;
    private ConnectionAbstract fSelectedConnection;

    public PanelScene(MainFrame mainFrame, NodePropertiesFrame nodePropsFrame) {
        fMainFrame = mainFrame;
        fNodePropsFrame = nodePropsFrame;
        fDiagram = new DiagramDeployment();

        oldMouseX = -1;
        oldMouseY = -1;
        fNewConnectionStart = null;
        fNewConnectionEnd = null;
        fDraggingDevice = null;
        fSelectedNode = null;
        fSelectedConnection = null;

        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                panelSceneMousePressed(evt);
            }
            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panelSceneMouseReleased(evt);
            }
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                panelSceneMouseClicked(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                panelSceneMouseDragged(evt);
            }
        });
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawDiagram((Graphics2D)g);
    }

    public Graphics2D getContext() {
        return (Graphics2D)getGraphics();
    }

    public void drawDiagram(Graphics2D context) {
        context.clearRect(0, 0, 2000, 2000);
        // Draw the diagram.
        fDiagram.draw(context);
        // Draw the new connection if exists.
        if (fNewConnectionStart != null && fNewConnectionEnd != null) {
            int x1 = (int)fNewConnectionStart.getX();
            int y1 = (int)fNewConnectionStart.getY();
            int x2 = (int)fNewConnectionEnd.getX();
            int y2 = (int)fNewConnectionEnd.getY();
            context.drawLine(x1, y1, x2, y2);
        }
    }

    public DiagramDeployment getDiagram() {
        return fDiagram;
    }

    public void setDiagram(DiagramDeployment diagram) {
        fDiagram = diagram;
        drawDiagram(getContext());
    }

    public void onDeleteClicked() {
        if (fSelectedNode != null) {
            fDiagram.removeNode(fSelectedNode);
        }
        if (fSelectedConnection != null) {
            fDiagram.removeConnection(fSelectedConnection);
        }
        fSelectedNode = null;
        fSelectedConnection = null;
        drawDiagram(getContext());
    }

    private void panelSceneMousePressed(java.awt.event.MouseEvent evt) {
        int activeTool = fMainFrame.selectedTool();
        int x = evt.getX();
        int y = evt.getY();

        fSelectedNode = null;
        fSelectedConnection = null;
        fDiagram.selectAll(false);

        if (activeTool == MainFrame.TOOL_SELECT) {
            fSelectedNode = fDiagram.findNodeAtPos(getContext(), x, y);
            if (fSelectedNode != null && fSelectedNode.type() == NodeAbstract.TYPE_DEVICE) {
                fDraggingDevice = (NodeDevice)fSelectedNode;
            }
            fSelectedConnection = fDiagram.findConnectionAtPos(getContext(), x, y, 7);

            if (fSelectedConnection != null || evt.getButton() != MouseEvent.BUTTON1) {
                fDraggingDevice = null;
                fSelectedNode = null;
            }

            // Is the selected item a node?
            if (fSelectedNode != null) {
                fSelectedNode.setSelected(true);
            }

            // Is the selected item a connection?
            if (fSelectedConnection != null) {
                NodeAbstract from = fSelectedConnection.getNodeFrom();
                NodeAbstract to = fSelectedConnection.getNodeTo();
                Point2D pFrom = from.getConnectionPoint(getContext(), to);
                Point2D pTo = to.getConnectionPoint(getContext(), from);
                Point2D pEvt = evt.getPoint();

                // Connection can be either re-directed or just selected.
                if (pEvt.distance(pFrom) <= 10) {
                    fDiagram.beginRedirectConnection(fSelectedConnection, to, from);
                    fNewConnectionStart = pTo;
                } else if (pEvt.distance(pTo) <= 10) {
                    fDiagram.beginRedirectConnection(fSelectedConnection, from, to);
                    fNewConnectionStart = pFrom;
                } else {
                    fSelectedConnection.setSelected(true);
                }
            }
        }
        if (fMainFrame.isConntectionTool(activeTool)) {
            // Start drawing a connection.
            fNewConnectionStart = new Point2D.Float(x, y);
        }

        drawDiagram(getContext());
    }

    private void panelSceneMouseReleased(java.awt.event.MouseEvent evt) {
        int activeTool = fMainFrame.selectedTool();
        int x = evt.getX();
        int y = evt.getY();

        if (fMainFrame.isNodeTool(activeTool)) {
            // Create a new node.
            int type = fMainFrame.toolToNodeType(activeTool);
            fDiagram.addNode(getContext(), type, x, y);
        }

        if (fMainFrame.isConntectionTool(activeTool)) {
            // Add a connection.
            int type = fMainFrame.toolToConnectionType(activeTool);
            fDiagram.addConnection(getContext(), type, (int)fNewConnectionStart.getX(), (int)fNewConnectionStart.getY(), x, y);
        }

        if (fDiagram.isRedirectingConnection()) {
            // Redirect a connection.
            fDiagram.endRedirectConnection(getContext(), x, y);
        }

        // Redraw the diagram.
        oldMouseX = -1;
        oldMouseY = -1;
        fNewConnectionStart = null;
        fNewConnectionEnd = null;
        fDraggingDevice = null;
        drawDiagram(getContext());

        // Finally, set the selection tool active.
        fMainFrame.setSelectedTool(MainFrame.TOOL_SELECT);
    }

    private void panelSceneMouseDragged(java.awt.event.MouseEvent evt) {
        int x = evt.getX();
        int y = evt.getY();

        int dx = 0;
        if (oldMouseX != -1) {
            dx = oldMouseX - x;
        }
        int dy = 0;
        if (oldMouseY != -1) {
            dy = oldMouseY - y;
        }

        if (fNewConnectionStart != null) {
            fNewConnectionEnd = evt.getPoint();
            drawDiagram(getContext());
        }
        if (fDraggingDevice != null && x >= 0 && x <= getWidth() && y >= 0 && y <= getHeight()) {
            fDraggingDevice.setX(fDraggingDevice.getX() - dx);
            fDraggingDevice.setY(fDraggingDevice.getY() - dy);
            fDraggingDevice.fitComponents(getContext());
            drawDiagram(getContext());
        }

        oldMouseX = x;
        oldMouseY = y;
    }

    private void panelSceneMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            NodeDevice dev = fDiagram.findDeviceAtPos(getContext(), evt.getX(), evt.getY());
            NodeRuntimeEnviroment re = fDiagram.findRuntimeEnviromentAtPos(getContext(), evt.getX(), evt.getY());
            ArrayList<NodeAbstract> editingNodes = new ArrayList<>();
            if (re != null) {
                editingNodes.add(re);
            }
            if (dev != null) {
                editingNodes.add(dev);
            }
            if (!editingNodes.isEmpty()) {
                fNodePropsFrame.setEditingNodes(editingNodes);
                fNodePropsFrame.setVisible(true);
            }
        }
    }
}
