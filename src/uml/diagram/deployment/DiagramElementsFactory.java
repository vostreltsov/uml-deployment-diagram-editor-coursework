package uml.diagram.deployment;

/**
 * @author Valeriy Streltsov
 */
public class DiagramElementsFactory {

    /**
     * Returns an instance of node of the given type.
     */
    public static NodeAbstract getNode(int type, int x, int y) {
        NodeAbstract result = null;
        switch (type) {
            case NodeAbstract.TYPE_DEVICE: {
                result = new NodeDevice(x, y);
                break;
            }
            case NodeAbstract.TYPE_RUNTIME_ENVIROMENT: {
                result = new NodeRuntimeEnviroment(x, y);
                break;
            }
            default: {
                // Should never get here.
                break;
            }
        }
        return result;
    }

    /**
     * Returns an instance of connection of the given type.
     */
    public static ConnectionAbstract getConnection(int type, NodeAbstract from, NodeAbstract to) {
        ConnectionAbstract result = null;
        switch (type) {
            case ConnectionAbstract.TYPE_ASSOCIATION: {
                result = new ConnectionAssociation(from, to);
                break;
            }
            case ConnectionAbstract.TYPE_DEPENDENCY: {
                result = new ConnectionDependency(from, to);
                break;
            }
            default: {
                // Should never get here.
                break;
            }
        }
        return result;
    }
}
