package uml.diagram.deployment;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

/**
 * @author Valeriy Streltsov
 */
public class MainFrame extends javax.swing.JFrame {

    public static final int TOOL_SELECT = 1;
    public static final int TOOL_NODE_DEVICE = 2;
    public static final int TOOL_NODE_RUNTIME_ENVIROMENT = 3;
    public static final int TOOL_EDGE_ASSOCIATION = 4;
    public static final int TOOL_EDGE_DEPENDENCY = 5;

    private NodePropertiesFrame fPropsFrame;

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        fPropsFrame = new NodePropertiesFrame();
        initComponents();
        fPropsFrame.setPanelScene(panelScene);

        //Set labels for tools.
        btnSelection.setText("<html><center>Selection</center></html>");
        btnNodeDevice.setText("<html><center>Device</center></html>");
        btnNodeRuntimeEnviroment.setText("<html><center>Runtime<br>enviroment</center></html>");
        btnEdgeAssociation.setText("<html><center>Association</center></html>");
        btnEdgeDependency.setText("<html><center>Dependency</center></html>");

        // Select the default tool.
        btnSelectionMouseReleased(null);
    }

    /**
     * Is the selected tool a node tool?
     */
    public boolean isNodeTool(int tool) {
        return tool == TOOL_NODE_DEVICE || tool == TOOL_NODE_RUNTIME_ENVIROMENT;
    }

    /**
     * Is the selected tool a connection tool?
     */
    public boolean isConntectionTool(int tool) {
        return tool == TOOL_EDGE_ASSOCIATION || tool == TOOL_EDGE_DEPENDENCY;
    }

    /**
     * Converts a tool type to a node type.
     */
    public int toolToNodeType(int tool) {
        switch (tool) {
            case TOOL_NODE_DEVICE: {
                return NodeAbstract.TYPE_DEVICE;
            }
            case TOOL_NODE_RUNTIME_ENVIROMENT: {
                return NodeAbstract.TYPE_RUNTIME_ENVIROMENT;
            }
            default: {
                return NodeAbstract.TYPE_ABSTRACT;
            }
        }
    }

    /**
     * Converts a tool type to a connection type.
     */
    public int toolToConnectionType(int tool) {
        switch (tool) {
            case TOOL_EDGE_ASSOCIATION: {
                return ConnectionAbstract.TYPE_ASSOCIATION;
            }
            case TOOL_EDGE_DEPENDENCY: {
                return ConnectionAbstract.TYPE_DEPENDENCY;
            }
            default: {
                return ConnectionAbstract.TYPE_ABSTRACT;
            }
        }
    }

    public int selectedTool() {
        if (btnNodeDevice.isSelected()) {
            return TOOL_NODE_DEVICE;
        } else if (btnNodeRuntimeEnviroment.isSelected()) {
            return TOOL_NODE_RUNTIME_ENVIROMENT;
        } else if (btnEdgeAssociation.isSelected()) {
            return TOOL_EDGE_ASSOCIATION;
        } else if (btnEdgeDependency.isSelected()) {
            return TOOL_EDGE_DEPENDENCY;
        } else {
            return TOOL_SELECT;
        }
    }

    public void setSelectedTool(int tool) {
        btnSelection.setSelected(false);
        btnNodeDevice.setSelected(false);
        btnNodeRuntimeEnviroment.setSelected(false);
        btnEdgeAssociation.setSelected(false);
        btnEdgeDependency.setSelected(false);
        switch (tool) {
            case TOOL_SELECT: {
                btnSelection.setSelected(true);
                break;
            }
            case TOOL_NODE_DEVICE: {
                btnNodeDevice.setSelected(true);
                break;
            }
            case TOOL_NODE_RUNTIME_ENVIROMENT: {
                btnNodeRuntimeEnviroment.setSelected(true);
                break;
            }
            case TOOL_EDGE_ASSOCIATION: {
                btnEdgeAssociation.setSelected(true);
                break;
            }
            case TOOL_EDGE_DEPENDENCY: {
                btnEdgeDependency.setSelected(true);
                break;
            }
            default: {
                btnSelection.setSelected(true);
                break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {
        panelTools = new javax.swing.JPanel();
        btnNodeDevice = new javax.swing.JToggleButton();
        btnNodeRuntimeEnviroment = new javax.swing.JToggleButton();
        btnSelection = new javax.swing.JToggleButton();
        btnEdgeAssociation = new javax.swing.JToggleButton();
        btnEdgeDependency = new javax.swing.JToggleButton();
        panelScene = new PanelScene(this, fPropsFrame);    //javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnNodeDevice.setText("D");
        btnNodeDevice.setFocusPainted(false);
        btnNodeDevice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnNodeDeviceMouseReleased(evt);
            }
        });

        btnNodeRuntimeEnviroment.setText("RE");
        btnNodeRuntimeEnviroment.setFocusPainted(false);
        btnNodeRuntimeEnviroment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnNodeRuntimeEnviromentMouseReleased(evt);
            }
        });

        btnSelection.setText("S");
        btnSelection.setFocusPainted(false);
        btnSelection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnSelectionMouseReleased(evt);
            }
        });

        btnEdgeAssociation.setText("A");
        btnEdgeAssociation.setFocusPainted(false);
        btnEdgeAssociation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEdgeAssociationMouseReleased(evt);
            }
        });

        btnEdgeDependency.setText("D");
        btnEdgeDependency.setFocusPainted(false);
        btnEdgeDependency.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnEdgeDependencyMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelToolsLayout = new javax.swing.GroupLayout(panelTools);
        panelTools.setLayout(panelToolsLayout);
        panelToolsLayout.setHorizontalGroup(
            panelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelToolsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSelection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnNodeDevice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelToolsLayout.createSequentialGroup()
                        .addComponent(btnNodeRuntimeEnviroment)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnEdgeAssociation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEdgeDependency, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelToolsLayout.setVerticalGroup(
            panelToolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelToolsLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(btnSelection)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNodeDevice)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNodeRuntimeEnviroment)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdgeAssociation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdgeDependency)
                .addContainerGap(463, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelSceneLayout = new javax.swing.GroupLayout(panelScene);
        panelScene.setLayout(panelSceneLayout);
        panelSceneLayout.setHorizontalGroup(
            panelSceneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 846, Short.MAX_VALUE)
        );
        panelSceneLayout.setVerticalGroup(
            panelSceneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jMenu1.setText("File");

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Open...");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Save...");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Export picture...");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);
        jMenu1.add(jSeparator1);

        jMenuItem1.setText("Exit");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);
        jMenuItem1.getAccessibleContext().setAccessibleName("menuExit");

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        jMenuItem2.setText("Remove selected");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(panelScene, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelTools, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelTools, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelScene, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>

    private void btnSelectionMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSelectionMouseReleased
        setSelectedTool(TOOL_SELECT);
    }//GEN-LAST:event_btnSelectionMouseReleased

    private void btnNodeDeviceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNodeDeviceMouseReleased
        setSelectedTool(TOOL_NODE_DEVICE);
    }//GEN-LAST:event_btnNodeDeviceMouseReleased

    private void btnNodeRuntimeEnviromentMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNodeRuntimeEnviromentMouseReleased
        setSelectedTool(TOOL_NODE_RUNTIME_ENVIROMENT);
    }//GEN-LAST:event_btnNodeRuntimeEnviromentMouseReleased

    private void btnEdgeAssociationMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEdgeAssociationMouseReleased
        setSelectedTool(TOOL_EDGE_ASSOCIATION);
    }//GEN-LAST:event_btnEdgeAssociationMouseReleased

    private void btnEdgeDependencyMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEdgeDependencyMouseReleased
        setSelectedTool(TOOL_EDGE_DEPENDENCY);
    }//GEN-LAST:event_btnEdgeDependencyMouseReleased

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        panelScene.onDeleteClicked();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // Save to file.
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fileChooser.getSelectedFile();
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
                out.writeObject(panelScene.getDiagram());
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // Load from file.
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fileChooser.getSelectedFile();
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                panelScene.setDiagram((DiagramDeployment)in.readObject());
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // Export to picture.
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fileChooser.getSelectedFile();
                BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g2d = bufferedImage.createGraphics();
                //g2d.setColor(Color.white);
                //g2d.fillRect(0, 0, panelScene.getWidth(), panelScene.getHeight());
                //g2d.setColor(Color.black);
                panelScene.getDiagram().draw(g2d);
                g2d.dispose();
                ImageIO.write(bufferedImage, "png", file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    private javax.swing.JToggleButton btnEdgeDependency;
    private javax.swing.JToggleButton btnEdgeAssociation;
    private javax.swing.JToggleButton btnNodeDevice;
    private javax.swing.JToggleButton btnNodeRuntimeEnviroment;
    private javax.swing.JToggleButton btnSelection;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private PanelScene /*javax.swing.JPanel*/ panelScene;
    private javax.swing.JPanel panelTools;
}
