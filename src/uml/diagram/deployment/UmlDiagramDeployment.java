package uml.diagram.deployment;

/**
 * @author Valeriy Streltsov
 */
public class UmlDiagramDeployment {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Hello, UML.
        MainFrame mf = new MainFrame();
        mf.setVisible(true);
    }
}
