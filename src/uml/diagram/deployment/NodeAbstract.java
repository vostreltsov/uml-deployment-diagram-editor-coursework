package uml.diagram.deployment;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Valeriy Streltsov
 */
public abstract class NodeAbstract implements Serializable {

    public static final int TYPE_ABSTRACT = 0;
    public static final int TYPE_DEVICE = 1;
    public static final int TYPE_RUNTIME_ENVIROMENT = 2;

    protected static final int MARGIN = 10;

    protected int fX;
    protected int fY;
    protected String fName;
    protected ArrayList<Artefact> fArtefacts;
    protected transient boolean fIsSelected;

    public NodeAbstract(int x, int y) {
        fArtefacts = new ArrayList();
        setX(x);
        setY(y);
        setName("");
    }

    /**
     * Returns type of this node.
     */
    abstract public int type();

    /**
     * Draws this node using the given context.
     */
    abstract public void draw(Graphics2D context);

    /**
     * Calculates the title, i.e. the first line in the node rectangle.
     */
    abstract public String title();

    /**
     * Calculates width of the graphical representation.
     */
    public int calculateWidth(Graphics2D context) {
        int max = 0;
        for (Artefact a : fArtefacts) {
            max = Math.max(a.calculateWidth(context), max);
        }
        return max + 2 * MARGIN;
    }

    /**
     * Calculates height of the graphical representation.
     */
    public int calculateHeight(Graphics2D context) {
        int result = 0;
        int fontHeight = context.getFontMetrics().getHeight();
        result += (fArtefacts.size() + 1) * fontHeight;
        return result + 2 * MARGIN;
    }

    /**
     * Sets the absolute x coordinate.
     */
    public final void setX(int x) {
        fX = x;
    }

    /**
     * Returns the absolute x coordinate.
     */
    public final int getX() {
        return fX;
    }

    /**
     * Sets the absolute y coordinate.
     */
    public final void setY(int y) {
        fY = y;
    }

    /**
     * Returns the absolute y coordinate.
     */
    public final int getY() {
        return fY;
    }

    /**
     * Sets name of the node.
     */
    public final void setName(String name) {
        fName = name;
    }

    /**
     * Returns name of the node.
     */
    public final String getName() {
        return fName;
    }

    /**
     * Adds an artefact to this node.
     */
    public void addArtefact(Artefact a) {
        if (a.getValue().length() > 0) {
            fArtefacts.add(a);
        }
    }

    /**
     * Removes an artefact from this node.
     */
    public void removeArtefact(Artefact a) {
        fArtefacts.remove(a);
    }

    /**
     * Returns all artefacts of this node.
     */
    public ArrayList<Artefact> getArtefacts() {
        return fArtefacts;
    }

    /**
     * Fits all inner components after changing position.
     */
    public void fitComponents(Graphics2D context) {
        FontMetrics fontMetrics = context.getFontMetrics();
        int x = MARGIN + fX;
        int y = fY + fontMetrics.getHeight() * 2; // + 1 for the label
        for (Artefact a : fArtefacts) {
            a.setX(x);
            a.setY(y);
            y += fontMetrics.getHeight();
        }

    }

    /**
     * Sets the selection/deselection of this node.
     */
    public void setSelected(boolean value) {
        fIsSelected = value;
    }

    /**
     * Returns the selection state of this node.
     */
    public boolean isSelected() {
        return fIsSelected;
    }

    /**
     * Returns the best connection point to connect this node with another.
     */
    public Point2D getConnectionPoint(Graphics2D context, NodeAbstract other) {
        Point2D result;
        double centerX = fX + calculateWidth(context) / 2;
        double centerY = fY + calculateHeight(context) / 2;
        double dx = other.fX - fX;
        double dy = other.fY - fY;
        double slope = dy / dx;

        if (dx == 0 && dy == 0) {
            result = new Point2D.Double(centerX, centerY);
        } else if (dx == 0 && dy > 0) {
            result = new Point2D.Double(centerX, fY + calculateHeight(context));
        } else if (dx == 0 && dy < 0) {
            result = new Point2D.Double(centerX, fY);
        } else if (dy > centerY && Math.abs(slope) > 1) {
            result = new Point2D.Double(centerX + (1 / slope) * (calculateWidth(context) / 2), centerY + calculateHeight(context) / 2);
        } else if (dy < centerY && Math.abs(slope) > 1) {
            if (other.fY > centerY) {
                result = new Point2D.Double(centerX + (1 / slope) * (calculateWidth(context) / 2), centerY + calculateHeight(context) / 2);
            } else {
                result = new Point2D.Double(centerX - (1 / slope) * (calculateWidth(context) / 2), centerY - calculateHeight(context) / 2);
            }
        } else if (Math.abs(slope) < 1 && dx > 0) {
            result = new Point2D.Double(centerX + calculateWidth(context) / 2, centerY + slope * (calculateHeight(context) / 2));
        } else {
            result = new Point2D.Double(centerX - calculateWidth(context) / 2, centerY - slope * (calculateHeight(context) / 2));
        }

        return result;
    }
}
