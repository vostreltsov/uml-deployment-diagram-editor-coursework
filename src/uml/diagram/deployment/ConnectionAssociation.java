package uml.diagram.deployment;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 * @author Valeriy Streltsov
 */
public class ConnectionAssociation extends ConnectionAbstract {

    public ConnectionAssociation() {
        this(null, null);
    }

    public ConnectionAssociation(NodeAbstract from, NodeAbstract to) {
        super(from, to);
    }

    @Override
    public int type() {
        return TYPE_ASSOCIATION;
    }

    @Override
    public void draw(Graphics2D context) {
        Color oldColor = context.getColor();
        if (isSelected()) {
            context.setColor(Color.RED);
        }
        Point2D pFrom = fNodeFrom.getConnectionPoint(context, fNodeTo);
        Point2D pTo = fNodeTo.getConnectionPoint(context, fNodeFrom);
        context.drawLine((int)pFrom.getX(), (int)pFrom.getY(), (int)pTo.getX(), (int)pTo.getY());
        context.setColor(oldColor);
    }
}
