package uml.diagram.deployment;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Valeriy Streltsov
 */
public class DiagramDeployment implements Serializable {

    private ArrayList<NodeDevice> fNodes;               // All top-level nodes.
    private ArrayList<ConnectionAbstract> fConnections; // All connections between nodes.
    private ConnectionAbstract fDraggingConnection;     // Connection being redirected.
    private NodeAbstract fDraggingConnectionNodeFrom;   // Fixed start node.
    private NodeAbstract fDraggingConnectionNodeTo;     // Old end node.

    public DiagramDeployment() {
        fNodes = new ArrayList();
        fConnections = new ArrayList();
        fDraggingConnection = null;
        fDraggingConnectionNodeFrom = null;
        fDraggingConnectionNodeTo = null;
    }

    /**
     * Begins a connection redirection.
     */
    void beginRedirectConnection(ConnectionAbstract connection, NodeAbstract from, NodeAbstract to) {
        fDraggingConnection = connection;
        fDraggingConnectionNodeFrom = from;
        fDraggingConnectionNodeTo = to;
        removeConnection(connection);
    }

    /**
     * Ends a connection redirection.
     */
    void endRedirectConnection(Graphics2D context, int x, int y) {
        NodeAbstract from = fDraggingConnectionNodeFrom;
        NodeAbstract to = findNodeAtPos(context, x, y);
        NodeAbstract toOld = fDraggingConnectionNodeTo;
        if (from != null && to != null && canBeConnected(from, to)) {
            if (from == fDraggingConnection.getNodeFrom()) {
                fDraggingConnection.setNodeTo(to);
            } else {
                fDraggingConnection.setNodeFrom(to);
            }
            addConnectionInner(fDraggingConnection);
        } else if (from != null && toOld != null) {
            addConnectionInner(fDraggingConnection);
        }
        fDraggingConnection = null;
        fDraggingConnectionNodeFrom = null;
        fDraggingConnectionNodeTo = null;
    }

    /**
     * Is there a redirection connection.
     */
    boolean isRedirectingConnection() {
        return fDraggingConnection != null;
    }

    /**
     * Selects or deselects all elements on the diagram.
     */
    public void selectAll(boolean selected) {
        for (NodeDevice node : fNodes) {
            node.setSelected(selected);
            for (NodeRuntimeEnviroment re : node.getRuntimeEnviroments()) {
                re.setSelected(selected);
            }
        }
        for (ConnectionAbstract c : fConnections) {
            c.setSelected(selected);
        }
    }

    /**
     * Draws this diagram using the given context.
     */
    public void draw(Graphics2D context) {
        for (NodeDevice node : fNodes) {
            node.draw(context);
        }
        for (ConnectionAbstract connection : fConnections) {
            connection.draw(context);
        }
    }

    /**
     * Checks if two nodes can be connected.
     */
    public static boolean canBeConnected(NodeAbstract from, NodeAbstract to) {
        return from != null && to != null && from != to && from.type() == to.type();
    }

    /**
     * Finds a connection containing a specified point.
     */
    public ConnectionAbstract findConnectionAtPos(Graphics2D context, int x, int y, int delta) {
        for (ConnectionAbstract c : fConnections) {
            Point2D pFrom = c.getNodeFrom().getConnectionPoint(context, c.getNodeTo());
            Point2D pTo = c.getNodeTo().getConnectionPoint(context, c.getNodeFrom());
            double d = distanceToLine(x, y, pFrom.getX(), pFrom.getY(), pTo.getX(), pTo.getY());
            if (d <= delta) {
                return c;
            }
        }
        return null;
    }

    /**
     * Finds a device node containing a specified point.
     */
    public NodeDevice findDeviceAtPos(Graphics2D context, int x, int y) {
        for (NodeDevice node : fNodes) {
            if (node.getX() <= x && x <= node.getX() + node.calculateWidth(context) &&
                node.getY() <= y && y <= node.getY() + node.calculateHeight(context)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Finds a runtime enviroment node containing a specified position.
     */
    public NodeRuntimeEnviroment findRuntimeEnviromentAtPos(Graphics2D context, int x, int y) {
        NodeDevice parent = findDeviceAtPos(context, x, y);
        if (parent != null) {
            return parent.findRuntimeEnviromentAtPos(context, x, y);
        }
        return null;
    }

    /**
     * Finds a node at pos with priority to runtime enviroment nodes.
     */
    public NodeAbstract findNodeAtPos(Graphics2D context, int x, int y) {
        NodeAbstract result = findRuntimeEnviromentAtPos(context, x, y);
        if (result == null) {
            result = findDeviceAtPos(context, x, y);
        }
        return result;
    }

    /**
     * Adds a node to the diagram.
     */
    public void addNode(Graphics2D context, int type, int x, int y) {
        // Find existing nodes at the given position.
        NodeDevice dev = findDeviceAtPos(context, x, y);
        if (dev != null && type == NodeAbstract.TYPE_RUNTIME_ENVIROMENT) {
            // Add runtime enviroment node.
            NodeAbstract node = DiagramElementsFactory.getNode(type, x, y);
            dev.addRuntimeEnviroment((NodeRuntimeEnviroment)node);
            dev.fitComponents(context);
        }
        if (type == NodeAbstract.TYPE_DEVICE) {
            // Add device node.
            NodeAbstract node = DiagramElementsFactory.getNode(type, x, y);
            fNodes.add((NodeDevice)node);
        }
    }

    /**
     * Removes a node from the diagram.
     */
    public void removeNode(NodeAbstract node) {
        // First, remove all incident connections.
        removeIncidentConnections(node);
        // Remove the node itself.
        if (node.type() == NodeAbstract.TYPE_DEVICE) {
            for (NodeRuntimeEnviroment re : ((NodeDevice)node).getRuntimeEnviroments()) {
                removeNode(re);
            }
            fNodes.remove((NodeDevice)node);
        }
        if (node.type() == NodeAbstract.TYPE_RUNTIME_ENVIROMENT) {
            for (NodeDevice dev : fNodes) {
                dev.removeRuntimeEnviroment((NodeRuntimeEnviroment)node);
            }
        }
    }

    public ArrayList<ConnectionAbstract> getConnections() {
        return fConnections;
    }

    /**
     * Adds a connection to the diagram.
     */
    public void addConnection(Graphics2D context, int type, int x1, int y1, int x2, int y2) {
        NodeAbstract from = findNodeAtPos(context, x1, y1);
        NodeAbstract to = findNodeAtPos(context, x2, y2);
        if (from == null || to == null || !canBeConnected(from, to)) {
            return;
        }
        ConnectionAbstract connection = DiagramElementsFactory.getConnection(type, from, to);
        addConnectionInner(connection);
    }

    /**
     * Removes a connection from the diagram.
     */
    public void removeConnection(ConnectionAbstract connection) {
        fConnections.remove(connection);
    }

    /**
     * Removes all incident connections.
     */
    public void removeIncidentConnections(NodeAbstract node) {
        ArrayList<ConnectionAbstract> connectionsToRemove = new ArrayList<>();
        for (ConnectionAbstract c : fConnections) {
            if (c.getNodeFrom() == node || c.getNodeTo() == node) {
                connectionsToRemove.add(c);
            }
        }
        for (ConnectionAbstract c : connectionsToRemove) {
            removeConnection(c);
        }
    }

    private void addConnectionInner(ConnectionAbstract connection) {
        for (ConnectionAbstract e : fConnections) {
            if ((e.getNodeFrom() == connection.getNodeFrom() && e.getNodeTo() == connection.getNodeTo()) ||
                (e.getNodeFrom() == connection.getNodeTo() && e.getNodeTo() == connection.getNodeFrom())) {
                return;
            }
        }
        fConnections.add(connection);
    }

    /**
     * Calculates the distance from a point to a line.
     */
    private double distanceToLine(double pX, double pY, double x0, double y0, double x1, double y1) {
        Line2D line = new Line2D.Double(x0, y0, x1, y1);
        return line.ptSegDist(new Point2D.Double(pX, pY));
    }
}
