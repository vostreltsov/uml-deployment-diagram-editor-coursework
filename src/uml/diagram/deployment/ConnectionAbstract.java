package uml.diagram.deployment;

import java.awt.Graphics2D;
import java.io.Serializable;
/**
 * @author Valeriy Streltsov
 */
public abstract class ConnectionAbstract implements Serializable {

    public static final int TYPE_ABSTRACT = 0;
    public static final int TYPE_ASSOCIATION = 1;
    public static final int TYPE_DEPENDENCY = 2;

    protected NodeAbstract fNodeFrom;
    protected NodeAbstract fNodeTo;
    protected transient boolean fIsSelected;

    public ConnectionAbstract() {
        this(null, null);
    }

    public ConnectionAbstract(NodeAbstract from, NodeAbstract to) {
        setNodeFrom(from);
        setNodeTo(to);
    }

    /**
     * Returns type of this connection.
     */
    abstract public int type();

    /**
     * Draws this node using the given context.
     */
    abstract public void draw(Graphics2D context);

    /**
     * Returns the start node.
     */
    public final NodeAbstract getNodeFrom() {
        return fNodeFrom;
    }

    /**
     * Returns the end node.
     */
    public final NodeAbstract getNodeTo() {
        return fNodeTo;
    }

    /**
     * Sets the start node.
     */
    public final void setNodeFrom(NodeAbstract node) {
        fNodeFrom = node;
    }

    /**
     * Sets the end node.
     */
    public final void setNodeTo(NodeAbstract node) {
        fNodeTo = node;
    }

    /**
     * Sets the selection/deselection of this connection.
     */
    public void setSelected(boolean value) {
        fIsSelected = value;
    }

    /**
     * Returns the selection state of this connection.
     */
    public boolean isSelected() {
        return fIsSelected;
    }
}
