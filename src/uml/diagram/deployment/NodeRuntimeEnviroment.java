package uml.diagram.deployment;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

/**
 * @author Valeriy Streltsov
 */
public class NodeRuntimeEnviroment extends NodeAbstract {

    public NodeRuntimeEnviroment(int x, int y) {
        super(x, y);
    }

    @Override
    public int type() {
        return TYPE_RUNTIME_ENVIROMENT;
    }

    @Override
    public void draw(Graphics2D context) {
        Color oldColor = context.getColor();
        if (isSelected()) {
            context.setColor(Color.RED);
        }
        FontMetrics fontMetrics = context.getFontMetrics();
        context.drawRect(fX, fY, calculateWidth(context), calculateHeight(context));
        context.drawString(title(), MARGIN + fX, fY + fontMetrics.getHeight());

        for (Artefact a : fArtefacts) {
            a.draw(context);
        }
        context.setColor(oldColor);
    }

    @Override
    public String title() {
        return "<runtime enviroment> " + getName();
    }

    @Override
    public int calculateWidth(Graphics2D context) {
        FontMetrics fontMetrics = context.getFontMetrics();
        int max = fontMetrics.stringWidth(title());
        return Math.max(super.calculateWidth(context), max + 2 * MARGIN);
    }
}
