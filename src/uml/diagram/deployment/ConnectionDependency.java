package uml.diagram.deployment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * @author Valeriy Streltsov
 */
public class ConnectionDependency extends ConnectionAbstract {

    public ConnectionDependency() {
        this(null, null);
    }

    public ConnectionDependency(NodeAbstract from, NodeAbstract to) {
        super(from, to);
    }

    @Override
    public int type() {
        return TYPE_DEPENDENCY;
    }

    @Override
    public void draw(Graphics2D context) {
        Color oldColor = context.getColor();
        Stroke oldStroke = context.getStroke();
        if (isSelected()) {
            context.setColor(Color.RED);
        }
        context.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));

        // Draw the line.
        Point2D pFrom = fNodeFrom.getConnectionPoint(context, fNodeTo);
        Point2D pTo = fNodeTo.getConnectionPoint(context, fNodeFrom);
        context.drawLine((int)pFrom.getX(), (int)pFrom.getY(), (int)pTo.getX(), (int)pTo.getY());

        // Draw the arrow.
        AffineTransform tx = new AffineTransform();
        tx.setToIdentity();

        Polygon arrowHead = new Polygon();
        arrowHead.addPoint(0, 5);
        arrowHead.addPoint(-5, -5);
        arrowHead.addPoint(5, -5);

        double angle = Math.atan2((int)pTo.getY() - (int)pFrom.getY(), (int)pTo.getX() - (int)pFrom.getX());
        tx.translate((int)pTo.getX(), (int)pTo.getY());
        tx.rotate((angle-Math.PI/2d));

        Graphics2D g = (Graphics2D)context.create();
        g.setTransform(tx);
        g.fill(arrowHead);
        g.dispose();

        // Reset original color and stroke.
        context.setColor(oldColor);
        context.setStroke(oldStroke);
    }
}
