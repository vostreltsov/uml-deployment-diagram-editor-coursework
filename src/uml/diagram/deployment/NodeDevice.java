package uml.diagram.deployment;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * @author Valeriy Streltsov
 */
public class NodeDevice extends NodeAbstract {

    private ArrayList<NodeRuntimeEnviroment> fRuntimeEnviroment;

    public NodeDevice(int x, int y) {
        super(x, y);
        fRuntimeEnviroment = new ArrayList();
    }

    @Override
    public int type() {
        return TYPE_DEVICE;
    }

    @Override
    public void draw(Graphics2D context) {
        Color oldColor = context.getColor();
        if (isSelected()) {
            context.setColor(Color.RED);
        }
        FontMetrics fontMetrics = context.getFontMetrics();
        context.drawRect(fX, fY, calculateWidth(context), calculateHeight(context));
        context.drawString(title(), MARGIN + fX, fY + fontMetrics.getHeight());
        for (Artefact a : fArtefacts) {
            a.draw(context);
        }
        for (NodeRuntimeEnviroment re : fRuntimeEnviroment) {
            re.draw(context);
        }
        context.setColor(oldColor);
    }

    @Override
    public String title() {
        return "<device> " + getName();
    }

    @Override
    public int calculateWidth(Graphics2D context) {
        int superWidth = super.calculateWidth(context);
        FontMetrics fontMetrics = context.getFontMetrics();
        int max = fontMetrics.stringWidth(title());
        for (NodeRuntimeEnviroment re : fRuntimeEnviroment) {
            max = Math.max(re.calculateWidth(context), max);
        }
        return Math.max(superWidth, max + 2 * MARGIN);
    }

    @Override
    public int calculateHeight(Graphics2D context) {
        int result = super.calculateHeight(context);
        for (NodeRuntimeEnviroment re : fRuntimeEnviroment) {
            result += MARGIN + re.calculateHeight(context);
        }
        return result;
    }

    /**
     * Finds a node containing a specified position.
     */
    public NodeRuntimeEnviroment findRuntimeEnviromentAtPos(Graphics2D context, int x, int y) {
        for (NodeRuntimeEnviroment node : fRuntimeEnviroment) {
            if (node.getX() <= x && x <= node.getX() + node.calculateWidth(context) &&
                node.getY() <= y && y <= node.getY() + node.calculateHeight(context)) {
                return node;
            }
        }
        return null;
    }

    public final ArrayList<NodeRuntimeEnviroment> getRuntimeEnviroments() {
        return fRuntimeEnviroment;
    }

    /**
     * Adds a runtime enviroment to this device.
     */
    public final void addRuntimeEnviroment(NodeRuntimeEnviroment node) {
        fRuntimeEnviroment.add(node);
    }

    /**
     * Removes a runtime enviroment from this node.
     */
    public final void removeRuntimeEnviroment(NodeRuntimeEnviroment node) {
        fRuntimeEnviroment.remove(node);
    }

    @Override
    public void fitComponents(Graphics2D context) {
        super.fitComponents(context);
        if (fRuntimeEnviroment == null) {
            return;
        }
        FontMetrics fontMetrics = context.getFontMetrics();
        int x = MARGIN + fX;
        int y = MARGIN + fY + fontMetrics.getHeight() * (fArtefacts.size() + 1);
        for (NodeRuntimeEnviroment re : fRuntimeEnviroment) {
            re.setX(x);
            re.setY(y);
            re.fitComponents(context);
            y += MARGIN + re.calculateHeight(context);
        }
    }
}
